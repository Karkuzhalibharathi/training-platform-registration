from rest_framework.permissions import BasePermission
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from .models import Student

class IsAdminUser(BasePermission):
    def has_permission(self, request, view):
        token = request.headers.get('Authorization')
        if not token:
            return False

        try:
            token_key = token.split()[1]
            token_obj = Token.objects.get(key=token_key)
            user = token_obj.user
            request.user = user
            return True
        except Token.DoesNotExist:
            return False

class IsStudent(BasePermission):
    def has_permission(self, request, view):
        student_id = request.query_params.get('id') or view.kwargs.get('pk')
        if not student_id:
            return False

        try:
            student = Student.objects.get(pk=student_id)
            return True
        except Student.DoesNotExist:
            return False