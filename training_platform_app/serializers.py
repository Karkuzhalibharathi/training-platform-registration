from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Student, Course, TrainingSchedule

class AdminRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
                    'id',
                    'first_name',
                    'email',
                    'password',
                    'username',
                ]
    def create(self,validated_data):

        user = User.objects.create_user(
                first_name  =  validated_data['first_name'],
                username    =  validated_data['username'],
                email       =  validated_data['email'],
                password    =  validated_data['password'],
            )
        return user

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['id', 'title', 'description', 'duration', 'created_at', 'updated_at']
        read_only_fields = ['created_at', 'updated_at']

class TrainingScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingSchedule
        fields = ['id', 'course', 'date', 'start_time', 'end_time', 'created_at', 'updated_at']
        read_only_fields = ['created_at', 'updated_at']

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['id', 'name', 'email','training_schedule', 'opted_in', 'created_at', 'updated_at']
        read_only_fields = ['created_at', 'updated_at']