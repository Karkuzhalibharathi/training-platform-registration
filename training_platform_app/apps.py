from django.apps import AppConfig


class TrainingPlatformAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'training_platform_app'
