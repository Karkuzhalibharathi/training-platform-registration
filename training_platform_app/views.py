import re
import string
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .permissions import IsAdminUser, IsStudent
from .serializers import (AdminRegisterSerializer, CourseSerializer, TrainingScheduleSerializer,
                          StudentSerializer)
from .models import Course, Student, TrainingSchedule
from rest_framework import status
from django.contrib.auth.hashers import check_password
from rest_framework.exceptions import NotFound
from django.contrib.auth import authenticate

class AdminRegisterView(APIView):

    def post(self,request):
        try:
            mandatory_fields = [
                                'username',
                                'email',
                                'name',
                                'password',
                                'confirm_password',
                            ]
            regex = re.compile('[_!#$%^&*()<>?/\|}{~:]')
            response_data = {}
            for i in mandatory_fields:                                                                                                               #input keys-validation
                if i not in request.data.keys():
                    return Response({
                    "status" : 0,
                    "message": f"{i} - is required"
                })
            if ("@" not in request.data['email'] or regex.search(request.data['email']) != None) and request.data['email'] != "":       #email-validation
                return Response({
                    "status" :  0,
                    "message":  "Invalid Email"
                })
            if request.data['password'] != request.data['confirm_password']:                                                            #password - confirm
                return Response({
                    "status" : 0,
                    "message": "Password mismatch"
                })
            if len(request.data['password']) < 8:                                                                         #password length set
                return Response({
                    "status"  : 0,
                    "message" : "Password must contains 8 characters"
                })
            if User.objects.filter(email = request.data['email']).exists():                                                            #email availablity
                return Response({
                    "status"    : 0,
                    "message"   : "Email already exists"
                })
            if ((bool(set.intersection(set(list(request.data['password'])),set(list(string.ascii_lowercase)))) == False) and
                (bool(set.intersection(set(list(request.data['password'])),set(list(string.ascii_uppercase)))) == False)) or \
                (bool(set.intersection(set(list(request.data['password'])),set(list(string.digits)))) == False):                         #password validation
                return Response({
                    "status"    : 0,
                    "message"   : "password should be alphanumeric"
                })

            request_data = {
                "first_name" : request.data['name'],
                "email"      : request.data['email'],
                "password"   : request.data['password'],
                "username"   : request.data['username']
            }
            serializer = AdminRegisterSerializer(data = request_data)
            if serializer.is_valid(raise_exception = True):
                serializer.save()
                token,created = Token.objects.get_or_create(
                                    user_id = serializer.data['id']
                                    )
                response_data = {
                    "id"        : serializer.data['id'],
                    "name"      : serializer.data['first_name'],
                    "email"     : serializer.data['email'],
                    "username"  : serializer.data['username'],
                    "token"     : token.key,
                    }
                return Response({
                    "status"    : 1,
                    "message"   : "created",
                    "data"      : response_data
                })
            else:
                return Response({
                    "status"    : 0,
                    "message"   : "registration failed"
                })
        except Exception:
            return Response({
                "status"    : 0,
                "error"     : "Something went wrong"
            })


class LoginAPIView(APIView):
    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        user = User.objects.filter(username=username).first()

        if user is not None and check_password(password, user.password):
            token, _ = Token.objects.get_or_create(user=user)
            return Response({'status': 1, 'message': 'Login Success',
                             'token': token.key}, status=status.HTTP_200_OK)
        else:
            return Response({'status':0,'message': 'Invalid credentials.'}, status=status.HTTP_401_UNAUTHORIZED)

class CourseListView(APIView):
    def get(self, request):
        courses = Course.objects.all()
        serializer = CourseSerializer(courses, many=True)
        return Response({'status': 1, 'message': 'Courses retrieved successfully',
                         'data': serializer.data}, status=status.HTTP_200_OK)

class CreateCourseView(APIView):
    permission_classes = [IsAdminUser]
    def post(self, request):
        serializer = CourseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'status': 1, 'message': 'Course created successfully',
                             'data': serializer.data}, status=status.HTTP_201_CREATED)
        return Response({'status': 0, 'message': 'Failed to create course', 'data': serializer.errors},
                        status=status.HTTP_400_BAD_REQUEST)

class CourseDetailView(APIView):

    def get_permissions(self):
        if self.request.method in ['PUT', 'DELETE']:
            return [IsAdminUser()]
        return []
    def get_object(self, pk):
        try:
            return Course.objects.get(pk=pk)
        except Course.DoesNotExist:
            raise NotFound("Course not found with the provided ID.")

    def get(self, request, pk):
        course = self.get_object(pk)
        serializer = CourseSerializer(course)
        return Response({'status': 1, 'message': 'Course retrieved successfully',
                         'data': serializer.data}, status=status.HTTP_200_OK)

    def put(self, request, pk):
        course = self.get_object(pk)
        serializer = CourseSerializer(course, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'status': 1, 'message': 'Course updated successfully',
                             'data': serializer.data}, status=status.HTTP_200_OK)
        return Response({'status': 0, 'message': 'Failed to update course',
                         'data': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        course = self.get_object(pk)
        course.delete()
        return Response({'status': 1, 'message': 'Course deleted successfully'},
                        status=status.HTTP_204_NO_CONTENT)


class TrainingScheduleListView(APIView):
    def get(self, request):
        schedules = TrainingSchedule.objects.all()
        serializer = TrainingScheduleSerializer(schedules, many=True)
        return Response(
            {'status': 1, 'message': 'Training schedules retrieved successfully',
             'data': serializer.data}, status=status.HTTP_200_OK)

class CreateTrainingScheduleView(APIView):
    permission_classes = [IsAdminUser]
    def post(self, request):
        serializer = TrainingScheduleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {'status': 1, 'message': 'Training schedule created successfully',
                 'data': serializer.data}, status=status.HTTP_201_CREATED)
        return Response({'status': 0, 'message': 'Failed to create training schedule',
                         'data': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class TrainingScheduleDetailView(APIView):

    def get_permissions(self):
        if self.request.method in ['PUT', 'DELETE']:
            return [IsAdminUser()]
        return []

    def get_object(self, pk):
        try:
            return TrainingSchedule.objects.get(pk=pk)
        except TrainingSchedule.DoesNotExist:
            raise NotFound("Training schedule not found with the provided ID.")

    def get(self, request, pk):
        schedule = self.get_object(pk)
        serializer = TrainingScheduleSerializer(schedule)
        return Response(
            {'status': 1, 'message': 'Training schedule retrieved successfully',
             'data': serializer.data}, status=status.HTTP_200_OK)

    def put(self, request, pk):
        schedule = self.get_object(pk)
        serializer = TrainingScheduleSerializer(schedule, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {'status': 1, 'message': 'Training schedule updated successfully',
                 'data': serializer.data}, status=status.HTTP_200_OK)
        return Response({'status': 0, 'message': 'Failed to update training schedule',
                         'data': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        schedule = self.get_object(pk)
        schedule.delete()
        return Response({'status': 1, 'message': 'Training schedule deleted successfully'},
                        status=status.HTTP_204_NO_CONTENT)


class StudentListView(APIView):

    permission_classes = [IsAdminUser]
    def get(self, request):
        students = Student.objects.all()
        serializer = StudentSerializer(students, many=True)
        return Response({'status': 1, 'message': 'Students retrieved successfully',
                         'data': serializer.data}, status=status.HTTP_200_OK)

class CreateStudentView(APIView):

    def post(self, request):
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            training_schedule = serializer.validated_data.get('training_schedule')
            if training_schedule:
                serializer.validated_data['opted_in'] = True
            serializer.save()
            return Response({'status': 1, 'message': 'Student created successfully',
                             'data': serializer.data}, status=status.HTTP_201_CREATED)
        return Response({'status': 0, 'message': 'Failed to create student',
                         'data': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class StudentDetailView(APIView):

    def get_permissions(self):
        if self.request.method == 'PUT':
            return [IsStudent() or IsAdminUser()]  # Either student or admin can update
        elif self.request.method == 'DELETE':
            return [IsAdminUser()]  # Only admin can delete
        return []

    def get_object(self, pk):
        try:
            return Student.objects.get(pk=pk)
        except Student.DoesNotExist:
            raise NotFound("Student not found with the provided ID.")

    def get(self, request, pk):
        student = self.get_object(pk)
        serializer = StudentSerializer(student)
        return Response({'status': 1, 'message': 'Student retrieved successfully',
                         'data': serializer.data}, status=status.HTTP_200_OK)

    def put(self, request, pk):
        student = self.get_object(pk)
        serializer = StudentSerializer(student, data=request.data)
        if serializer.is_valid():
            training_schedule = serializer.validated_data.get('training_schedule')
            if training_schedule:
                serializer.validated_data['opted_in'] = True
            serializer.save()
            return Response({'status': 1, 'message': 'Student updated successfully',
                             'data': serializer.data}, status=status.HTTP_200_OK)
        return Response({'status': 0, 'message': 'Failed to update student',
                         'data': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        student = self.get_object(pk)
        student.delete()
        return Response({'status': 1, 'message': 'Student deleted successfully'},
                        status=status.HTTP_204_NO_CONTENT)

class StudentInOutOfTrainingView(APIView):
    permission_classes = [IsStudent]

    def get_object(self, pk):
        try:
            return Student.objects.get(pk=pk)
        except Student.DoesNotExist:
            raise NotFound("Student not found with the provided ID.")

    def post(self, request, pk):
        student = self.get_object(pk)
        if student.training_schedule:
            student.opted_in = not student.opted_in
            student.save()
            serializer = StudentSerializer(student)
            message = 'Student enter in to training' if student.opted_in else 'student out from training'
            return Response({'status': 1, 'message': message, 'data': serializer.data})
        else:
            return Response({'status': 0, 'message': 'Training schedule not allocated for the student'}, status=status.HTTP_400_BAD_REQUEST)