from django.urls import path
from .views import (AdminRegisterView, LoginAPIView, CreateCourseView, CourseListView,
                    CourseDetailView, CreateTrainingScheduleView, TrainingScheduleListView,
                    TrainingScheduleDetailView, CreateStudentView, StudentListView,
                    StudentDetailView, StudentInOutOfTrainingView)

urlpatterns = [
    path('admin-register/', AdminRegisterView.as_view(),name = "registration"),
    path('login/', LoginAPIView.as_view(), name='login'),
    path('create-course/', CreateCourseView.as_view(), name='create-course'),
    path('course-list/', CourseListView.as_view(), name='course-list'),
    path('courses/<int:pk>/', CourseDetailView.as_view(), name='course-detail'),
    path('create-training-schedule/', CreateTrainingScheduleView.as_view(), name='create-schedule'),
    path('schedule-list/', TrainingScheduleListView.as_view(), name='schedule-list'),
    path('schedule-detail/<int:pk>/', TrainingScheduleDetailView.as_view(), name='schedule-detail'),
    path('create-student/', CreateStudentView.as_view(), name='create-student'),
    path('student-list/', StudentListView.as_view(), name='student-list'),
    path('student-detail/<int:pk>/', StudentDetailView.as_view(), name='student-detail'),
    path('students/<int:pk>/opt-in-out/', StudentInOutOfTrainingView.as_view(), name='student-opt-in-out'),
]


